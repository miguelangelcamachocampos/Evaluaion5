package utng.data;

import org.hibernate.HibernateException;
import utng.model.Company;


public class CompanyDAO extends DAO<Company> {

    public CompanyDAO() {
        super(new Company());
    }

    
    public Company getOneById(Company company) throws HibernateException {
        return super.getOneById(company.getIdCompany()); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
    
}
