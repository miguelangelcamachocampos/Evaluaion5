package utng.handler;

import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import utng.data.DrinkDAO;
import utng.model.Drink;

@ManagedBean
@SessionScoped
public class DrinkBean implements Serializable{
    private List<Drink>drinks;
    private Drink drink;
    public DrinkBean(){}

    public List<Drink> getDrinks() {
        return drinks;
    }

    public void setDrinks(List<Drink> drinks) {
        this.drinks = drinks;
    }

    public Drink getDrink() {
        return drink;
    }

    public void setDrink(Drink drink) {
        this.drink = drink;
    }
    
    
    public String toList(){
        DrinkDAO dao = new DrinkDAO();
        try {
            drinks=dao.getAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "Drinks";
    }
    
    public String remove(){
         DrinkDAO dao = new DrinkDAO();
        try {
            dao.delete(drink);
            drinks=dao.getAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "Remove";
    }
    
    public String start(){
        drink= new Drink();
        return "Start";
    }
    
    public String save(){
        DrinkDAO dao = new DrinkDAO();
        try {
            if(drink.getIdDrink()!= 0){
                dao.update(drink);
            }else {
                dao.insert(drink);
            }
            drinks=dao.getAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "Save";
    }
    
    public String cancel(){
    return "Cancel";
    }
    
    public String edit(Drink drink){
        this.drink=drink;
        return "Edit";
    }
    
}
