package utng.handler;

import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import utng.data.DrinkDAO;
import utng.data.CompanyDAO;
import utng.model.Drink;
import utng.model.Company;

@ManagedBean(name="companyBean") 
@SessionScoped
public class CompanyBean implements Serializable{
    private List<Company> companies;
    private Company company;
    private List<Drink> drinks;
    public CompanyBean(){
        company = new Company();
        company.setDrink(new Drink());
    }

    public List<Company> getCompanies() {
        return companies;
    }
    public void setCompanies(List<Company> companies) {
        this.companies = companies;
    }
    public Company getCompany() {
        return company;
    }
    public void setCompany(Company company) {
        this.company = company;
    }
    public List<Drink> getDrinks() {
        return drinks;
    }
    public void setDrinks(List<Drink> drinks) {
        this.drinks = drinks;
    }
    
     public String toList(){
        CompanyDAO dao = new CompanyDAO();
        try {
            companies=dao.getAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "Companies";
    }
    
    public String remove(){
         CompanyDAO dao = new CompanyDAO();
        try {
            dao.delete(company);
            companies=dao.getAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "Remove";//no me queda claro si este es es lo que devuelve 
    }
    
    public String start(){
        company= new Company();
        company.setDrink(new Drink());
        try {
            drinks= new DrinkDAO().getAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "Start";
    }
    
    public String save(){
        CompanyDAO dao = new CompanyDAO();
        try {
            if(company.getIdCompany()!= 0){
                dao.update(company);
            }else {
                dao.insert(company);
            }
            companies=dao.getAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "Save";
    }
    
    public String cancel(){
    return "Cancel";
    }
    
    public String edit(Company company){
        this.company=company;
        try {
            drinks = new DrinkDAO().getAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "Edit";
    }
    
   
    
    
}
