package utng.model;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="company")
public class Company implements Serializable{
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id_company")
    private Long idCompany;
    @Column(name="name_company", length=10)
    private String nameCompany;
    @Column(length=40)
    private String city;
    @Column(length=40)
     private String price;
    @Column(length = 40)
    private String cp;
    @Column(length = 40)
    private String route;

      
    @ManyToOne()
    @JoinColumn(name="id_drink")
    private Drink drink;

    public Company(Long idCompany, String nameCompany, String city, String price, String cp, String route, Drink drink) {
        this.idCompany = idCompany;
        this.nameCompany = nameCompany;
        this.city = city;
        this.price = price;
        this.cp = cp;
        this.route = route;
        this.drink = drink;
    }
 
    public Company() {
        this.idCompany=0L;
    }

    public Long getIdCompany() {
        return idCompany;
    }

    public void setIdCompany(Long idCompany) {
        this.idCompany = idCompany;
    }

    public String getNameCompany() {
        return nameCompany;
    }

    public void setNameCompany(String nameCompany) {
        this.nameCompany = nameCompany;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public Drink getDrink() {
        return drink;
    }

    public void setDrink(Drink drink) {
        this.drink = drink;
    }
    
}
